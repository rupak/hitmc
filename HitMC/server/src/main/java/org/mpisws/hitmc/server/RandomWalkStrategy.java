package org.mpisws.hitmc.server;

import org.mpisws.hitmc.api.Event;

import java.util.*;

public class RandomWalkStrategy implements SchedulingStrategy {

    private final Random random;

    private final RandomWalkStatistics statistics;

    private final Set<Event> events = new HashSet<>();

    public RandomWalkStrategy(final Random random, final RandomWalkStatistics statistics) {
        this.random = random;
        this.statistics = statistics;
    }

    @Override
    public void add(final Event event) {
        events.add(event);
        if (nextEventPrepared && nextEvent == null) {
            nextEventPrepared = false;
        }
    }

    private boolean nextEventPrepared = false;
    private Event nextEvent = null;

    @Override
    public boolean hasNextEvent() {
        if (!nextEventPrepared) {
            prepareNextEvent();
        }
        return nextEvent != null;
    }

    @Override
    public Event nextEvent() {
        if (!nextEventPrepared) {
            prepareNextEvent();
        }
        nextEventPrepared = false;
        return nextEvent;
    }

    private void prepareNextEvent() {
        final List<Event> enabled = new ArrayList<>();
        for (final Event event : events) {
            if (event.isEnabled()) {
                enabled.add(event);
            }
        }
        statistics.reportNumberOfEnabledEvents(enabled.size());

        nextEvent = null;
        if (enabled.size() > 0) {
            final int i = random.nextInt(enabled.size());
            nextEvent = enabled.get(i);
            events.remove(nextEvent);
        }
        nextEventPrepared = true;
    }
}
